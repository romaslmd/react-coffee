"use strict";

import React from "react";

module.exports = React.createClass({
    displayName: "Copyrights",

    currentYear() {
        return new Date().getFullYear();
    },

    render() {
        return(
            <div className="text-center">
                Copyrights © {this.currentYear()} All rights reserved. Roman Solomud.
                <br />
                Reproduction in whole or in part in any form or medium without express written permission of The News Reporter Inc. is prohibited. The trade marks and images used in the design are the copyright of their respective owners. They are used only for display purpose.
            </div>
        );
    }
});
