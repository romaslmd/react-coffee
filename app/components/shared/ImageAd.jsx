"use strict";

import React from "react";
import { Link } from "react-router";
import { Image } from "react-bootstrap";

module.exports = React.createClass({
    displayName: "ImageAd",

    getDefaultProps() {
        return { src: "/assets/ad-fullwidth.png", link: null };
    },

    componentWillMount() {
        this.setState(this.props);
    },

    hasLink() {
        !!this.state.link
    },

    renderLinkedAd() {
        return(<a href={this.state.link} target="_blank">{this.renderAd()}</a>);
    },

    renderAd() {
        return(<Image src={this.state.src} responsive />);
    },

    render() {
        return(<div className="image-ad">{this.hasLink() ? this.renderLinkedAd() : this.renderAd()}</div>);
    }
});
