"use strict";

import React from "react";
import { Link } from "react-router";
import { Row, Col, Image } from "react-bootstrap";

module.exports = React.createClass({
    displayName: "CategoryBlockItem",

    getDefaultProps() {
        return { id: null, title: null, thumbnail: null, description: null, author_name: null, comments_count: null, created_at: null };
    },

    componentWillMount() {
        this.setState(this.props);
    },

    render() {
        return(
            <Col className="category-preview-item" md={12}>
                <div><Image src={this.state.thumbnail} responsive /></div>
                <h5><Link to="post" params={{id: this.state.id}}>{this.state.title}</Link></h5>
            </Col>
        );
    }
});
