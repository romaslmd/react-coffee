"use strict";

import React from "react";
import { Link } from "react-router";
import { Row, Col, Image, Modal } from "react-bootstrap";

module.exports = React.createClass({
    displayName: "CategoryGalleryItem",

    getDefaultProps() {
        return { id: null, title: null, thumbnail: null, description: null, author_name: null, comments_count: null, created_at: null };
    },

    getInitialState() {
        return { togglePreview: false };
    },

    componentWillMount() {
        this.setState(this.props);
    },

    close() {
        this.setState(this.getInitialState());
    },

    togglePreview() {
        this.setState({togglePreview: true});
    },

    render() {
        return(
            <div>
                <Image src={this.state.thumbnail} onClick={this.togglePreview} responsive />
                <Modal  className="gallery-preview" bsSize="lg" show={this.state.togglePreview} onHide={this.close}>
                    <Modal.Body>
                        <Row>
                            <Col md={6} lg={6}><Link to="post" params={{id: this.state.id}}><Image src={this.state.thumbnail} responsive /></Link></Col>
                            <div>
                                <h2><Link to="post" params={{id: this.state.id}}>{this.state.title}</Link></h2>
                                <p>{this.state.description}</p>
                            </div>
                        </Row>
                    </Modal.Body>
                </Modal>
            </div>
        );
    }
});
