"use strict";

import React from "react";
import { Input, Button } from "react-bootstrap";

module.exports = React.createClass({
    displayName: "NewsSubscription",

    getDefaultProps() {
        return { email: null, name: null, errors: {} };
    },

    componentWillMount() {
        this.setState(this.props);
    },

    updateName(e) {
        this.setState({name: e.target.value});
    },

    updateEmail(e) {
        this.setState({email: e.target.value});
    },

    submit() {
        console.log("Submit news subscription", this.state.name, this.state.email)
    },

    render() {
        return(
            <div className="well news-subscription">
                <h2>Sign Up for Newsletter</h2>
                <Input placeholder="Name" value={this.state.name} onChange={this.updateName} type="text" ref="input" />
                <Input placeholder="Email address" value={this.state.email} onChange={this.updateEmail} type="email" ref="input" />
                <Button className="btn-important" onClick={this.submit}>Submit</Button>
                <div className="help-block text-center important">We value your privacy!</div>
            </div>
        );
    }
});
