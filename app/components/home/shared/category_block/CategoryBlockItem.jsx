"use strict";

import React from "react";
import { Link } from "react-router";
import { Row, Col, Image } from "react-bootstrap";

module.exports = React.createClass({
    displayName: "CategoryBlockItem",

    getDefaultProps() {
        return { id: null, title: null, thumbnail: null, description: null, author_name: null, comments_count: null, created_at: null };
    },

    componentWillMount() {
        this.setState(this.props);
    },

    render() {
        return(
            <Col className="category-post" md={4} xs={6}>
                <div><Image src={this.state.thumbnail} responsive /></div>
                <h5>{this.state.title}</h5>
                <p>{this.state.description}</p>
                <Link className="readmore-link" to="post" params={{id: this.state.id}}>Read more</Link>
            </Col>
        );
    }
});
