"use strict";

import React from "react";
import Time from 'react-time'
import { Link } from "react-router";
import { Row, Col } from "react-bootstrap";

module.exports = React.createClass({
    displayName: "CategoryPopularItem",

    getDefaultProps() {
        return { id: null, title: null, thumbnail: null, description: null, author_name: null, comments_count: null, created_at: null };
    },

    componentWillMount() {
        this.setState(this.props);
    },

    render() {
        return(
            <Col className="category-popular-item" md={12}>
                <span><Time value={new Date(this.state.created_at)} format="MMM Do YYYY"/></span>
                <h5>{this.state.title}<Link className="post-link" to="post" params={{id: this.state.id}}>Read more</Link></h5>
                <div className="boundary"></div>
            </Col>
        );
    }
});
