"use strict";

import React from "react";
import _ from "lodash";
import { Link } from "react-router";
import { Row, Col } from "react-bootstrap";

import CategoryGalleryItem from "./category_gallery/CategoryGalleryItem.jsx";

module.exports = React.createClass({
    displayName: "CategoryGallery",

    getDefaultProps() {
        return {
            id: null,
            title: 'Gallery',
            posts: [
                { id: 1, thumbnail: '/assets/post-thumbnail.png', title: 'Lorem ipsum dolor sit amet, consectetur', description: 'Nulla quis lorem neque, mattis venenatis lectus. In interdum ullamcorper dolor eu mattis.', author_name: 'John Doe', comments_count: 5, created_at: 1452381904717 },
                { id: 2, thumbnail: '/assets/post-thumbnail.png', title: 'Lorem ipsum dolor sit amet, consectetur', description: 'Nulla quis lorem neque, mattis venenatis lectus. In interdum ullamcorper dolor eu mattis.', author_name: 'John Doe', comments_count: 5, created_at: 1452381904717 },
                { id: 3, thumbnail: '/assets/post-thumbnail.png', title: 'Lorem ipsum dolor sit amet, consectetur', description: 'Nulla quis lorem neque, mattis venenatis lectus. In interdum ullamcorper dolor eu mattis.', author_name: 'John Doe', comments_count: 5, created_at: 1452381904717 },
                { id: 4, thumbnail: '/assets/post-thumbnail.png', title: 'Lorem ipsum dolor sit amet, consectetur', description: 'Nulla quis lorem neque, mattis venenatis lectus. In interdum ullamcorper dolor eu mattis.', author_name: 'John Doe', comments_count: 5, created_at: 1452381904717 },
                { id: 5, thumbnail: '/assets/post-thumbnail.png', title: 'Lorem ipsum dolor sit amet, consectetur', description: 'Nulla quis lorem neque, mattis venenatis lectus. In interdum ullamcorper dolor eu mattis.', author_name: 'John Doe', comments_count: 5, created_at: 1452381904717 },
                { id: 6, thumbnail: '/assets/post-thumbnail.png', title: 'Lorem ipsum dolor sit amet, consectetur', description: 'Nulla quis lorem neque, mattis venenatis lectus. In interdum ullamcorper dolor eu mattis.', author_name: 'John Doe', comments_count: 5, created_at: 1452381904717 }
            ] };
    },

    componentWillMount() {
        this.setState(this.props);
    },

    renderPost(post) {
        return(
            <Col key={post.id} md={4}>
                <CategoryGalleryItem {...post} />
            </Col>
        );
    },

    render() {
        return(
            <div className="category-box">
                <Row>
                    <Col className="category-title" md={12}>
                        <h2>{this.state.title}</h2>
                        <Link to="category" params={{id: this.state.id}}>More</Link>
                    </Col>
                </Row>
                <Row className="category-gallery">
                    {_.map(this.state.posts, this.renderPost)}
                </Row>
            </div>
        );
    }
});
