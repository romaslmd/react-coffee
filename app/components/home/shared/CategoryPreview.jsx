"use strict";

import React from "react";
import _ from "lodash";
import { Link } from "react-router";
import { Row, Col } from "react-bootstrap";

import CategoryPreviewItem from "./category_preview/CategoryPreviewItem.jsx";

module.exports = React.createClass({
    displayName: "CategoryPreview",

    getDefaultProps() {
        return {
            id: null,
            title: 'editorial',
            posts: [
                { id: 1, thumbnail: '/assets/post-image.png', title: 'Lorem ipsum dolor sit amet, consectetur', description: 'Nulla quis lorem neque, mattis venenatis lectus. In interdum ullamcorper dolor eu mattis.', author_name: 'John Doe', comments_count: 5, created_at: 1452381904717 },
                { id: 2, thumbnail: '/assets/post-image.png', title: 'Lorem ipsum dolor sit amet, consectetur', description: 'Nulla quis lorem neque, mattis venenatis lectus. In interdum ullamcorper dolor eu mattis.', author_name: 'John Doe', comments_count: 5, created_at: 1452381904717 },
                { id: 3, thumbnail: '/assets/post-image.png', title: 'Lorem ipsum dolor sit amet, consectetur', description: 'Nulla quis lorem neque, mattis venenatis lectus. In interdum ullamcorper dolor eu mattis.', author_name: 'John Doe', comments_count: 5, created_at: 1452381904717 }
            ] };
    },

    componentWillMount() {
        this.setState(this.props);
    },

    renderPosts(post) {
        return <CategoryPreviewItem key={post.id} {...post} />;
    },

    render() {
        return(
            <div className="category-preview">
                <Row>
                    <Col className="category-title" md={12}>
                        <h2>{this.state.title}</h2>
                    </Col>
                </Row>
                <Row className="category-posts">
                    {_.map(this.state.posts, this.renderPosts)}
                </Row>
            </div>
        );
    }
});
