"use strict";

import React from "react";
import { Link } from "react-router";
import { Row, Col } from "react-bootstrap";
import TimeAgo from "react-timeago";

module.exports = React.createClass({
    displayName: "CategoryRecentItem",

    getDefaultProps() {
        return { id: null, title: null, thumbnail: null, description: null, author_name: null, comments_count: null, created_at: null };
    },

    componentWillMount() {
        this.setState(this.props);
    },

    render() {
        return(
            <Col className="category-recent-item" md={12}>
                <h5>{this.state.title}</h5>
                <p>{this.state.description}</p>
                <Row>
                    <Col className="valign-middle" md={6} xs={6}><Link className="post-link" to="post" params={{id: this.state.id}}>Read more</Link></Col>
                    <Col className="valign-middle post-date" md={6} xs={6}><TimeAgo date={this.state.created_at} /></Col>
                </Row>
                <div className="boundary"></div>
            </Col>
        );
    }
});
