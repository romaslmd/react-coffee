"use strict";

import React from "react";
import { Link } from "react-router";
import { Row, Col } from "react-bootstrap";

module.exports = React.createClass({
    displayName: "CategoryArticleItem",

    getDefaultProps() {
        return { id: null, title: null, thumbnail: null, description: null, author_name: null, comments_count: null, created_at: null };
    },

    componentWillMount() {
        this.setState(this.props);
    },

    render() {
        return(
            <Col md={4}>
                <h3><Link to="post" params={{id: this.state.id}}>{this.state.title}</Link></h3>
                <p>{this.state.description}</p>
                <div className="item-meta">
                    by <span className="important">{this.state.author_name}</span>
                    <span className="separator">|</span>
                    {this.state.comments_count} comments
                </div>
            </Col>
        );
    }
});
