"se strict";

import React from "react";
import { Link } from "react-router";
import { Row, Col } from "react-bootstrap";

import CategoryArticleItem from "./category_article/CategoryArticleItem.jsx";

module.exports = React.createClass({
    displayName: "CategoryArticle",

    getDefaultProps() {
        return {
            id: null,
            title: 'Articles from around the world',
            posts: [
                { id: 1, thumbnail: '/assets/post-thumbnail.png', title: 'Lorem ipsum dolor sit amet, consectetur', description: 'Nulla quis lorem neque, mattis venenatis lectus. In interdum ullamcorper dolor eu mattis.', author_name: 'John Doe', comments_count: 5, created_at: 1452381904717 },
                { id: 2, thumbnail: '/assets/post-thumbnail.png', title: 'Lorem ipsum dolor sit amet, consectetur', description: 'Nulla quis lorem neque, mattis venenatis lectus. In interdum ullamcorper dolor eu mattis.', author_name: 'John Doe', comments_count: 8, created_at: 1452381904717 },
                { id: 3, thumbnail: '/assets/post-thumbnail.png', title: 'Lorem ipsum dolor sit amet, consectetur', description: 'Nulla quis lorem neque, mattis venenatis lectus. In interdum ullamcorper dolor eu mattis.', author_name: 'John Doe', comments_count: 15, created_at: 1452381904717 },
                { id: 4, thumbnail: '/assets/post-thumbnail.png', title: 'Lorem ipsum dolor sit amet, consectetur', description: 'Nulla quis lorem neque, mattis venenatis lectus. In interdum ullamcorper dolor eu mattis.', author_name: 'John Doe', comments_count: 15, created_at: 1452381904717 },
                { id: 5, thumbnail: '/assets/post-thumbnail.png', title: 'Lorem ipsum dolor sit amet, consectetur', description: 'Nulla quis lorem neque, mattis venenatis lectus. In interdum ullamcorper dolor eu mattis.', author_name: 'John Doe', comments_count: 15, created_at: 1452381904717 },
                { id: 6, thumbnail: '/assets/post-thumbnail.png', title: 'Lorem ipsum dolor sit amet, consectetur', description: 'Nulla quis lorem neque, mattis venenatis lectus. In interdum ullamcorper dolor eu mattis.', author_name: 'John Doe', comments_count: 36, created_at: 1452381904717 }
            ] };
    },

    componentWillMount() {
        this.setState(this.props);
    },

    renderArticles(post) {
        return(<CategoryArticleItem key={post.id} {...post} />);
    },

    render() {
        return(
            <div className="category-box">
                <Row>
                    <Col className="category-title" md={12}>
                        <h2>{this.state.title}</h2>
                        <Link to="category" params={{id: this.state.id}}>More</Link>
                    </Col>
                </Row>
                <Row className="category-articles">
                    {_.map(this.state.posts, this.renderArticles)}
                </Row>
            </div>
        );
    }
});
