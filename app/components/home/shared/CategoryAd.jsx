"use strict";

import React from "react";
import { Link } from "react-router";
import { Row, Col } from "react-bootstrap";

import CategoryAdItem from "./category_ad/CategoryAdItem.jsx";

module.exports = React.createClass({
    displayName: "CategoryAd",

    getDefaultProps() {
        return {
            id: 1, title: "Ad",
            posts: [
                { id: 1, thumbnail: '/assets/post-thumbnail.png', title: "Lorem ipsum dolor sit amet conse ctetur adipiscing elit", description: "Nulla quis lorem neque, mattis venen atis lectus. In interdum ull amcorper dolor eu mattis.", author_name: 'John Doe', comments_count: 5, created_at: 1452381854496 },
                { id: 2, thumbnail: '/assets/post-thumbnail.png', title: "Lorem ipsum dolor sit amet conse ctetur adipiscing elit", description: "Nulla quis lorem neque, mattis venen atis lectus. In interdum ull amcorper dolor eu mattis.", author_name: 'John Doe', comments_count: 5, created_at: 1452381895425 }
            ]
        };
    },

    componentWillMount() {
        this.setState(this.props);
    },

    renderPosts(post) {
        return(<Col key={post.id} md={12}><CategoryAdItem {...post}/></Col>);
    },
    render() {
        return(
            <div className="category-ad">
                <Row>
                    <Col className="category-title" md={12}>
                        <h2>{this.state.title}</h2>
                    </Col>
                </Row>
                <Row className="category-ads">
                    {_.map(this.state.posts, this.renderPosts)}
                </Row>
            </div>
        );
    }
});
