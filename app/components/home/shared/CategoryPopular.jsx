"use strict";

import React from "react";
import { Link } from "react-router";
import { Row, Col } from "react-bootstrap";

import CategoryPopularItem from "./category_popular/CategoryPopularItem.jsx";

module.exports = React.createClass({
    displayName: "CategoryPopular",

    getDefaultProps() {
        return {
            id: 1, title: "Popular",
            posts: [
                { id: 1, thumbnail: '/assets/post-thumbnail.png', title: "Lorem ipsum dolor sit amet conse ctetur adipiscing elit", description: "Nulla quis lorem neque, mattis venen atis lectus. In interdum ull amcorper dolor eu mattis.", author_name: 'John Doe', comments_count: 5, created_at: 1452381854496 },
                { id: 2, thumbnail: '/assets/post-thumbnail.png', title: "Lorem ipsum dolor sit amet conse ctetur adipiscing elit", description: "Nulla quis lorem neque, mattis venen atis lectus. In interdum ull amcorper dolor eu mattis.", author_name: 'John Doe', comments_count: 5, created_at: 1452381895425 },
                { id: 3, thumbnail: '/assets/post-thumbnail.png', title: "Lorem ipsum dolor sit amet conse ctetur adipiscing elit", description: "Nulla quis lorem neque, mattis venen atis lectus. In interdum ull amcorper dolor eu mattis.", author_name: 'John Doe', comments_count: 5, created_at: 1452381904717 },
                { id: 4, thumbnail: '/assets/post-thumbnail.png', title: "Lorem ipsum dolor sit amet conse ctetur adipiscing elit", description: "Nulla quis lorem neque, mattis venen atis lectus. In interdum ull amcorper dolor eu mattis.", author_name: 'John Doe', comments_count: 5, created_at: 1452381854496 },
                { id: 5, thumbnail: '/assets/post-thumbnail.png', title: "Lorem ipsum dolor sit amet conse ctetur adipiscing elit", description: "Nulla quis lorem neque, mattis venen atis lectus. In interdum ull amcorper dolor eu mattis.", author_name: 'John Doe', comments_count: 5, created_at: 1452381895425 },
                { id: 6, thumbnail: '/assets/post-thumbnail.png', title: "Lorem ipsum dolor sit amet conse ctetur adipiscing elit", description: "Nulla quis lorem neque, mattis venen atis lectus. In interdum ull amcorper dolor eu mattis.", author_name: 'John Doe', comments_count: 5, created_at: 1452381904717 }
            ]
        };
    },

    componentWillMount() {
        this.setState(this.props);
    },

    renderPosts(post) {
        return(<CategoryPopularItem key={post.id} {...post}/>);
    },
    render() {
        return(
            <div className="category-popular">
                <Row>
                    <Col className="category-title" md={12}>
                        <h2>{this.state.title}</h2>
                    </Col>
                </Row>
                <Row className="category-posts">
                    {_.map(this.state.posts, this.renderPosts)}
                </Row>
                <Row>
                    <Col md={12}><Link className="category-link" to="category" params={{id: this.state.id}}>More</Link></Col>
                </Row>
            </div>
        );
    }
});
