"use strict";

import React from "react";
import { Image } from "react-bootstrap";

module.exports = React.createClass({
    displayName: "CategoryAdItem",

    getDefaultProps() {
        return { id: null, title: null, thumbnail: null, description: null, author_name: null, comments_count: null, created_at: null };
    },

    getInitialState() {
        return { togglePreview: false };
    },

    componentWillMount() {
        this.setState(this.props);
    },

    close() {
        this.setState(this.getInitialState());
    },

    togglePreview() {
        this.setState({togglePreview: true});
    },

    render() {
        return(
            <div className="category-ad-item">
                <Image src={this.state.thumbnail} onClick={this.togglePreview} responsive />
            </div>
        );
    }
});
