"use strict";

import React from "react";
import { Row } from "react-bootstrap";

import CategoryRecent from "./shared/CategoryRecent.jsx";
import CategoryPreview from "./shared/CategoryPreview.jsx";

module.exports = React.createClass({
    displayName: "MiddleColumn",

    render() {
        return(
            <Row>
                <CategoryRecent />
                <CategoryPreview />
            </Row>
        );
    }
});
