"use strict";

import React from "react";
import { Row, Col } from "react-bootstrap";

import ImageAd from "../shared/ImageAd.jsx";
import NewsSubscription from "./shared/NewsSubscription.jsx";
import CategoryPopular from "./shared/CategoryPopular.jsx";
import CategoryAd from "./shared/CategoryAd.jsx";

module.exports = React.createClass({
    displayName: "SideBar",

    getDefaultProps() {
        return {};
    },

    componentWillMount() {
        this.setState(this.props);
    },

    render() {
        return(
            <div className="homepage-sidebar">
                <Row>
                    <Col md={12}><ImageAd src="/assets/column-ad.png" /></Col>
                </Row>
                <Row>
                    <Col md={12}><NewsSubscription /></Col>
                </Row>
                <Row>
                    <Col md={12}><CategoryPopular /></Col>
                </Row>
                <Row>
                    <Col md={12}><ImageAd src="/assets/column-ad.png" /></Col>
                </Row>
                <Row>
                    <Col md={12}><CategoryAd /></Col>
                </Row>
            </div>
        );
    }
});
