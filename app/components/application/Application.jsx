"use strict";

import React from "react";
import { Grid, Row, Col } from "react-bootstrap";

import Header from "../header/Header.jsx";
import Footer from "../footer/Footer.jsx";

module.exports = React.createClass({
    displayName: "Application",

    render() {
        return(
            <div className="body-wrapper fill">
                <Grid fluid>
                    <Row className="row-fluid">
                        <Col md={10} mdOffset={1}>
                            <Header />
                            {this.props.children}
                            <Footer />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
});
