"use strict";

import React from "react";
import { IndexLink } from "react-router";
import { Grid, Row, Col, Image } from "react-bootstrap";

import PrimaryNavigation from "./PrimaryNavigation.jsx";
import SearchBar from "./SearchBar.jsx";

module.exports = React.createClass({
    displayName: "Header",

    render() {
        return(
            <div>
                <Row className="content-container container-header">
                    <Col md={3}>
                        <IndexLink to="/">
                            <img className="img-responsive" src="/assets/logo.png" />
                        </IndexLink>
                    </Col>
                    <Col className="header-banner hidden-xs" md={5}>
                        <Image src="/assets/468x60.gif" responsive />
                    </Col>
                    <Col md={4}><SearchBar /></Col>
                </Row>
                <PrimaryNavigation />
            </div>
        );
    }
});
