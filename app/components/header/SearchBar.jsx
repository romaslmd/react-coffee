"use strict";

import React from "react";
import { Navigation } from "react-router";
import { Button, Row, Col, Input, Glyphicon } from "react-bootstrap";

module.exports = React.createClass({
    displayName: "SearchBar",
    mixins: [Navigation],

    getInitialState() {
        return { search: "" };
    },

    querySearch() {
        return this.state.search;
    },

    handleChange() {
        this.setState({search: this.refs.input.getValue()});
    },

    handleSearch() {
        this.transitionTo("/", { query: this.state.search });
    },

    render() {
        return(
            <div>
                <div className="social-box">
                    <ul className="social-links text-right">
                        <li><a className="social-twitter" href=""></a></li>
                        <li><a className="social-facebook" href=""></a></li>
                        <li><a className="social-rss" href=""></a></li>
                    </ul>
                </div>
                <div className="search-box">
                    <Input
                        ref="input" type="text" value={this.querySearch()}
                        placeholder="Search keyword.." onChange={this.handleChange}
                        buttonAfter={<Button onClick={this.handleSearch}><Glyphicon glyph="search" /></Button>} />
                </div>
            </div>
        );
    }
});
