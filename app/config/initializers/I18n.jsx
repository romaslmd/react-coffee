import YAML from "yamljs";
import i18n from 'i18next/lib';
import LanguageDetector from 'i18next-browser-languagedetector/lib';

const LANGUAGES_AVAILABLE = ['en'];

var resources = [];
for(var locale in LANGUAGES_AVAILABLE)
    resources[locale] = YAML.load("../locales/"+locale+".yml")[locale];

i18n.use(LanguageDetector).init({
    fallbackLng: 'en',
    ns: ['common'],
    defaultNS: 'common',
    debug: true,
    interpolation: {
        escapeValue: false
    },
    resources: resources
});

module.exports = i18n;
